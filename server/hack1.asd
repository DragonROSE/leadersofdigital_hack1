(asdf:defsystem :hack1
    :description "System for hakaton"
    :version "0.0.1"
    :author "Visualization Laboratory"
    :depends-on (:restas
                 :restas-directory-publisher)
;    :serial t
    :components ((:file "packages")
		 (:file "utils" :depends-on ("packages"))
		 (:file "db" :depends-on ("packages"))
		 (:file "core" :depends-on ("packages" "utils" "db"))
                 (:file "main" :depends-on ("packages" "utils" "db" "core"))))
