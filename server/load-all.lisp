(defun get-system-path()
  (merge-pathnames "leadersofdigital_hack1/server/" (user-homedir-pathname)))

(defun get-source-path (file)
  (merge-pathnames file (get-system-path)))

(dolist (source '("start-dev.lisp" "load.lisp" "start.lisp"))
  (load (get-source-path source)))

