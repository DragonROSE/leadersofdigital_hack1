(in-package :hack1)

(defparameter *system-dir* (asdf:system-source-directory :hack1))

(defun get-system-dir()
  *system-dir*)

(defun get-source-path (file)
  (merge-pathnames file (get-system-dir)))

(defun get-static-path ()
  (merge-pathnames "static/" (get-system-dir)))

(defun get-static-internal-path ()
  (merge-pathnames "static-internal/" (get-system-dir)))

(defun get-file-path (filename)
  (truename (concatenate 'string (namestring (get-static-path)) filename)))

(defun get-file-internal-path (filename)
  (truename (concatenate 'string (namestring (get-static-internal-path)) filename)))

(defun json-to-entities (&key data-file field constructor adder fields-spec
			 &aux users-str users-obj users args)
  (setf users-str (uiop:read-file-string (get-file-internal-path data-file)))
  (setf users-obj (parse users-str))
  (setf users (rest (assoc field (rest users-obj) :test 'string-equal)))
  (dolist (user-obj users)
      (let ((cur-user (rest user-obj)))
	(flet ((get-prop (user prop)
		 (cdr (assoc prop user :test #'equalp))))
	  (setf args (let (args)
		       (dolist (spec fields-spec)
			 (destructuring-bind (key prop)
			     spec
			   (push key args)
			   (push (get-prop cur-user prop) args)))
		       (nreverse args)))
	  (format *slime-output* "Object args: ~S" args)
	  (funcall adder (apply constructor args))
	  )))
  )

(defun entities-to-json(field fields-spec hash-entities)
  (jsown:to-json
   `(:OBJ
     (,Field ,@(let (res)
		 (hash-table-values
		  hash-entities
		   ;(get-tasks-obj);(get-users-obj)
		   (lambda (key val &aux cur-obj)
		     (declare (ignore key))
		     (dolist (spec fields-spec)
		       (destructuring-bind (prop method)
			   spec
			 (push (cons prop (funcall method val))
			       cur-obj)))
		     (push `(:obj ,@(nreverse cur-obj))
			   res)))
		  (nreverse res))))))

(defun hash-table-values (hash func)
  (loop for key being the hash-keys of hash
     using (hash-value value)
     do (funcall func key value)))
