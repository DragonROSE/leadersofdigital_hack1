(restas:define-module #:restas.hello-world
  (:use :cl :restas :hack1 :hack1-database :jsown))

(in-package #:restas.hello-world)

(restas:debug-mode-on)

(restas:mount-module -homedir- (#:restas.directory-publisher)
  (:url "/static/")
  (restas.directory-publisher:*directory* (namestring (get-static-path)))
  (restas.directory-publisher:*autoindex* t))

(restas:mount-module -images- (#:restas.directory-publisher)
  (:url "/images/")
  (restas.directory-publisher:*directory*
   (merge-pathnames "images/" (namestring (get-static-path))))
  (restas.directory-publisher:*autoindex* t))

(defparameter *slime-output* *standard-output*)

(defun get-session-user-data ()
  (hunchentoot:session-value 'user-data))

(defun save-user-data (data)
  (setf (hunchentoot:session-value 'user-data) data))

(defun switch-authorized(authorized-p)
  (setf (hunchentoot:session-value 'authorized-p) authorized-p))

(defun check-authorized()
  (hunchentoot:session-value 'authorized-p))
  
(restas:define-route main ("/")
  (if hunchentoot:*session*
      (format *slime-output* "~%session-id: ~S" (hunchentoot:session-id hunchentoot:*session*))
      (print "<no-session>" *slime-output*))
  (format *slime-output* "~%session user-data: ~S" (get-session-user-data))
  (truename (get-file-path "index.html")))

(restas:define-route create-initiative ("/actions/create-initiative" :method :get)
  (if (check-authorized)
      (truename (get-file-internal-path "taskcreate.html"))))

(defun get-actual-page (access)
  (case access
    (:user (truename (get-file-internal-path "User.html")))
    (:admin (truename (get-file-internal-path "Admin.html")))))

(setf *slime-output* *standard-output*)

(restas:define-route auth-main-authorized ("/auth/main" :method :get)
  ;(break "here data: ~s" (get-session-user-data))
  (step (if (check-authorized) 
      (let* ((access (getf (get-session-user-data) :access)))
	(if access
	    (get-actual-page access)
	    (truename (get-file-internal-path "unauthorized.html"))))
      (truename (get-file-internal-path "unauthorized.html")))))

(defun prepare-access (access)
  (cond ((string-equal access "user") :user)
	((string-equal access "admin") :admin)))

(restas:define-route auth-main ("/auth/main" :method :post)
  (format *slime-output* "~%post-parameters: ~S" (hunchentoot:post-parameters*))
  (let ((name (hunchentoot:post-parameter "usrname"))
	(pass (hunchentoot:post-parameter "psw"))
	(access (hunchentoot:post-parameter "access")))
    (setf access (prepare-access access))
    (if (check-user-p name pass access)
	(progn
	  ;(check-authorized)
	  (save-user-data (get-user name))
	  (format *slime-output* "~%user-data: ~S" (get-session-user-data))
					;(truename (get-file-internal-path "main.html")))
	  (switch-authorized t)
	  (get-actual-page access))
	(progn
	  (save-user-data nil)
	  (format *slime-output* "~%user-data: ~S" (get-session-user-data))
	  (truename (get-file-internal-path "unauthorized.html"))
	  ;(truename (get-file-internal-path "unauthorized.html"))
	  ))))

(restas:define-route auth-logout ("/auth/logout")
  (save-user-data nil)
  (format *slime-output* "~%user-data: ~S" (get-session-user-data))
  (switch-authorized nil)
  (truename (get-file-internal-path "logout.html")))

(restas:define-route auth-register ("/auth/register" :method :post)
  ;(break)
  (format *slime-output* "~%post-parameters: ~S" (hunchentoot:post-parameters*))
  (let ((name (hunchentoot:post-parameter "usrname"))
	(pass (hunchentoot:post-parameter "psw"))
	(access (hunchentoot:post-parameter "access"))
	user-data)
    (setf access (prepare-access access))
    (format *slime-output* "~%user props: ~S" (list name pass access))
    (register-user name pass access)
    (switch-authorized t)
    (setf user-data (get-user-data name))
    (save-user-data user-data)
    (format *slime-output* "~%session-id: ~S" (hunchentoot:session-id hunchentoot:*session*))
    (format *slime-output* "~%session user-data: ~S" (get-session-user-data))
    (truename (get-file-internal-path "registered.html"))))

;;;; Api
(restas:define-route users ("/api/users")
  (setf (hunchentoot:content-type*) "text/json")
  (uiop:read-file-string (get-file-internal-path "users.json")))

(restas:define-route tasks ("/api/tasks")
  (setf (hunchentoot:content-type*) "text/json")
  (uiop:read-file-string (get-file-internal-path "tasks.json"))
  ;(tasks-to-json)
  )

#|
(restas:define-route ajax ("/api/test/:(id1)/:(id2)")
  (setf (hunchentoot:content-type*) "text/json")
  "{\"asdf\": 42}")
|#
