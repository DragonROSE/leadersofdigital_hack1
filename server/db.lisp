(in-package :hack1-database)

(defstruct user
  id name id-vote credentials)

(defstruct status
  new working done)

(defstruct task
  id id-user name description type status datetime-created)

(defstruct links-profile
  id id-user type-profile session-hash)

(defstruct votes-user-tasks
  id ID_User ID_Task)

(defparameter *users* (make-hash-table :test 'equal))
(defparameter *tasks* (make-hash-table :test 'equal))
(defparameter *links-profiles* (make-hash-table :test 'equal))

(defparameter *auth* (make-hash-table :test 'equalp))
;; Init
(setf (gethash "user" *auth*) '(:id 20 :pass "user" :name "user" :access :user))
(setf (gethash "admin" *auth*) '(:id 20 :pass "admin" :name "admin" :access :admin))
      
;(setf (get '*auth* :table) *auth*)

(defparameter *slime-output* *standard-output*)

(defun add-user (name user-data)
;  (break "add ~s" (list name user-data))
  (format *slime-output* "~%user-pass: ~S" (list name user-data))
  (setf (gethash name *auth*) user-data)
;  (break "auth: ~s" (gethash name *auth*))
  )

;(setf (gethash "test" *auth*) '(:id 1 :pass "pass"))

(defparameter *slime-output* *standard-output*)

(defun get-user (name)
  (print "~~~~~~~~~~~" *slime-output*)
  (print *auth* *slime-output*)
  (gethash name *auth*))

(defmethod add-user-obj ((user user))
  (setf (gethash (user-id user) *users*) user))

(defmethod add-task-obj ((task task))
  (setf (gethash (task-id task) *tasks*) task))

(defun get-users-obj()
  *users*)

(defun get-tasks-obj()
  *tasks*)



