(in-package :hack1)

(defparameter *next-user-id* 1)

(defun gen-user-id()
  (prog1 *next-user-id* (incf *next-user-id*)))

(defparameter *slime-output* *standard-output*)

(defun register-user (name password access &aux data)
  (format *slime-output* "~%pass: ~S" password)
  (setf data (list :id (gen-user-id) :pass password :name name :access access))
  (format *slime-output* "~%register data: ~S" data)
  (add-user name data)
  ;(break "~s" (gethash "user" hack1-database::*auth*))
  (format *slime-output* "~%saved data: ~S" (get-user name))
  )

(defun check-user-p (name password req-access &aux user-data pass cur-access)
  (when (string= password "")
    (return-from check-user-p nil))
  (setf user-data (get-user name))
  (setf cur-access (getf user-data :access))
  (format *slime-output* "~%db user-data: ~S" user-data)
  (format *slime-output* "~%request access: ~S cur-access: ~S" req-access cur-access)
  (setf pass (getf user-data :pass))
  (and
   (eq req-access cur-access)
   (string= pass password)))

(defun get-user-data (name &aux all-data tmp)
  (setf all-data (get-user name))
  (setf tmp (copy-list all-data))
  (remf tmp :pass)
  tmp)


#|
(json-to-entities :data-file "users.json"
		  :field "Users"
		  :constructor 'make-user
		  :fields-spec '(
				 (:id "ID")
				 (:name "Name")
				 (:id-vote "ID_Vote")
				 (:credentials "Credentials")))
|#

(defun get-users()
  (json-to-entities :data-file "users.json"
		  :field "Users"
		  :constructor 'make-user
		  :adder 'add-user-obj
		  :fields-spec '(
				 (:id "ID")
				 (:name "Name")
				 (:id-vote "ID_Vote")
				 (:credentials "Credentials"))))
;(get-users)

(defun users-to-json ()
  (entities-to-json "Users"
		    '(("ID" user-id)
		      ("Name" user-name)
		      ("ID_Vote" user-id-vote)
		      ("Credientials" user-credentials))
		    (get-users-obj)))

;(users-to-spec)
(defun get-tasks()
  (json-to-entities :data-file "tasks.json"
		  :field "Tasks"
		  :constructor 'make-task
		  :adder 'add-task-obj
		  :fields-spec '(
				 (:id "ID")
				 (:id-user "ID_User")
				 (:name "Name")
				 (:description "Description")
				 (:type "Type")
				 (:status "Status")
				 (:datetime-created "DateTime_Created")
				 )))
;(get-tasks)
(defun tasks-to-json ()
  (entities-to-json "Tasks"
		    '(
		      ("ID" task-id)
		      ("ID_User" task-id-user)
		      ("Name" task-name)
		      ("Description" task-description)
		      ("Type" task-type)
		      ("Status" task-status)
		      ("DateTime_Created" task-datetime-created)
		      )
		    (get-tasks-obj)))

;(tasks-to-json)

;;;;;;;;;;; Initialization ;;;;;;;;;;;
(get-users)
(get-tasks)

    
