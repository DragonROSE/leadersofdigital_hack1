(defpackage :hack1-database
  (:use :cl)
  (:export #:add-user #:get-user #:*auth*
	   #:add-user-obj #:add-task-obj #:get-users-obj #:get-tasks-obj
	   #:make-user #:user-id #:user-name #:user-id-vote #:user-credentials
	   #:make-status #:status-new #:status-working #:status-done
	   #:make-task #:task-id #:task-id-user #:task-name #:task-description #:task-type #:task-status #:task-datetime-created
	   #:make-links-profile #:links-profile-id #:links-profile-id-user #:links-profile-type-profile #:links-profile-session-hash
	   #:make-votes-user-tasks #:votes-user-tasks-id #:votes-user-tasks-id-user #:votes-user-tasks-id-task
	   ))

(defpackage :hack1
  (:use :cl :hack1-database :jsown)
  (:export #:get-static-path #:get-file-path #:get-file-internal-path
	   #:gen-user-id #:register-user #:check-user-p
	   #:get-user-data #:users-to-json #:tasks-to-json
	   ))



